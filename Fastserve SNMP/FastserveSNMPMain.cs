﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharedLib;
using SnmpSharpNet;

namespace Fastserve_SNMP
{
    public class FastserveSNMPMain
    {
        private NameValueCollection _appSettings;
        private List<SNMPSensor> _sensorList = new List<SNMPSensor>();
        private string[] _fastserve_hosts;
        private string _hostname;

        public FastserveSNMPMain(string arg)
        {
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            _fastserve_hosts = _appSettings["fastserve_hosts"].Split(',');

            string profile = "";
            #if (HLV)
                                        profile = "hlv";
            #endif
            #if (TST)
                                        profile = "tst";
            #endif
            #if (DHG)
                                        profile = "dhg";
            #endif

            Logger._refID = System.Environment.MachineName;
            Logger.Connect();
            _hostname = System.Environment.MachineName + "." + System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToLower();


            Logger.WriteConsoleAndLog(MsgType.INFO, null, $"####### Init profile:{profile} #######");
            string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            
            
            foreach(string Fastserve_Host in _fastserve_hosts)
            {
                string Sensorfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"", $"{Fastserve_Host}.json");
                List<SNMPSensor> ThisList = JsonConvert.DeserializeObject<List<SNMPSensor>>(File.ReadAllText(Sensorfile));
                ThisList.ForEach(x => x.hostname = Fastserve_Host);
                // Assume all sensors are Ok on startup, they wil get there real values in a second
                ThisList.ForEach(x => x.last_ok_state = DateTime.Now);
                _sensorList.AddRange(ThisList);
            }
        }

        public void ConsoleStart()
        {
            Forever();
        }

        public void TaskStart()
        {
            Task.Factory.StartNew(() => Forever());
            Logger.WriteConsoleAndLog(MsgType.INFO, null, $"############### Start Service: {_hostname}. SNMP ###############");
        }

        public void TaskStop()
        {
            Logger.WriteConsoleAndLog(MsgType.INFO, null, $"############### Stop Service: {_hostname}. SNMP ###############");
        }

        public void Forever()
        {
            CancellationTokenSource cancelSource = new CancellationTokenSource();
            try
            {
                do
                {
                    Logger.Connect();
                    Logger.RotateLog();
                    Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Start ###############");

                    // update the list of sensors with changes made in file.
                    _sensorList = UpdateSensorList(_sensorList);

                    // Make a new list with only the enabled sensors.
                    List<SNMPSensor> _enabledSensorList = _sensorList.Where(x => x.enabled == true).ToList();

                    List<SNMPSensor> _allSensorList = new List<SNMPSensor>();
                    foreach (string Host in _fastserve_hosts)
                    {
                        // filter only the sensors for this host
                        List<SNMPSensor> Sensors = _enabledSensorList.Where(x => x.hostname == Host).ToList();
                        _allSensorList.AddRange(ReadSensors(Host, Sensors));
                    }


                    foreach (SNMPSensor Sensor in _allSensorList)
                    {
                        string PRTGMessage = PRTGRestClient.ComposePRTGMessage(Sensor);
                        PrtgResponse SubmitResponse = PRTGRestClient.SubmitSensor(Sensor.prtg_id, PRTGMessage);
                        if(SubmitResponse != null)
                        {
                            Logger.WriteConsoleAndLog(MsgType.INFO, null, $"Host:{Sensor.hostname.PadRight(21, ' ')}, Sensor:{Sensor.description.PadRight(20, ' ')},  State:{Sensor.state.ToString().PadRight(4, ' ')},  Prtgresponse:{SubmitResponse.status} Matchingsensor:{SubmitResponse.MatchingSensors}");
                        }                        
                    }
                }
                while (cancelSource.Token.WaitHandle.WaitOne(15 * 60 * 1000) || true);    // wait 29 minutes before diving into the loop again.
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Exeption", ex.ToString());
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Exeption", $"{ex.Message}");
                Environment.Exit(1);
            }
        }

        private List<SNMPSensor> UpdateSensorList(List<SNMPSensor> InternalList)
        {
            List<SNMPSensor> NewList = new List<SNMPSensor>();
            // Retrieve the sensors on file
            foreach (string Fastserve_Host in _fastserve_hosts)
            {
                string Sensorfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"", $"{Fastserve_Host}.json");
                List<SNMPSensor> ThisList = JsonConvert.DeserializeObject<List<SNMPSensor>>(File.ReadAllText(Sensorfile));
                ThisList.ForEach(x => x.hostname = Fastserve_Host);
                NewList.AddRange(ThisList);
            }

            // update enabled state and alarm Treshhold with possible changed values in sensor files.
            foreach(SNMPSensor Sensor in InternalList)
            {
                SNMPSensor MatchingSensor = NewList.FirstOrDefault(x => (x.hostname == Sensor.hostname && x.oid == Sensor.oid));
                if(MatchingSensor != null)
                {
                    Sensor.enabled = MatchingSensor.enabled;
                    Sensor.alarm_treshhold_minutes = MatchingSensor.alarm_treshhold_minutes;
                }
                else
                {
                    // remove sensor from list?
                }
            }
            return InternalList;
        }

        private List<SNMPSensor> ReadSensors(string HostName, List<SNMPSensor> SensorList)
        {
            SimpleSnmp snmp = new SimpleSnmp(HostName, "public");
            if (!snmp.Valid)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"SNMP agent on host {HostName} name/ip address is invalid.");
                return SensorList;
            }

            string[] OidList = SensorList.Select(x => "." + x.oid).ToArray();
            Dictionary<Oid, AsnType> bulkResult = snmp.Get(SnmpVersion.Ver2, OidList);
            if (bulkResult == null)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "SNMP collection result is null");
                return SensorList;
            }


            for (int i = 0; i < SensorList.Count(); i++)
            {
                SensorList[i].hostname = HostName;
                SensorList[i].rawvalue = bulkResult.ElementAt(i).Value.ToString();
                if (SensorList[i].type == Type.ok_nok)
                {
                    int value = Convert.ToInt32(SensorList[i].rawvalue);
                    if (value == 1)
                    {
                        SensorList[i].last_ok_state = DateTime.Now;
                        SensorList[i].state = State.Ok;
                    }
                    else if (value == 2)
                    {
                        SensorList[i].state = State.Nok;
                    }
                    else
                    {
                        SensorList[i].state = State.Unknown;
                    }
                }
                else if (SensorList[i].type == Type.fanspeed_min_max)
                {
                    int value = Convert.ToInt32(SensorList[i].rawvalue);
                    if (value > Lookups.fanspeed_min_max.Min() && value < Lookups.fanspeed_min_max.Max())
                    {
                        SensorList[i].last_ok_state = DateTime.Now;
                        SensorList[i].state = State.Ok;
                        SensorList[i].displayvalue = value.ToString();
                    }
                    else
                    {
                        SensorList[i].state = State.Nok;
                        SensorList[i].displayvalue = value.ToString();
                    }
                }
            }
            return SensorList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fastserve_SNMP
{
    class SNMPSensor
    {
        public string hostname { get; set; }
        public string oid { get; set; }
        public string description { get; set; }
        public Type type { get; set; }
        public string rawvalue { get; set; }
        public string displayvalue { get; set; }
        public State state { get; set; }
        public string action_required { get; set; }
        public int alarm_treshhold_minutes { get; set; }  // Minutes to wait after last_ok_state before a Nok state raises an alarm
        public DateTime last_ok_state { get; set; } // updates when state is ok.
        public bool enabled { get; set; } // updates when state is ok.
        public string prtg_id { get; set; }
    }

    enum State
    {
        Unknown,
        Ok,
        Nok
    }

    enum Type
    {
        ok_nok,
        fanspeed_min_max
    }

    static class Lookups
    {
        public static Dictionary<string, string> ok_nok = new Dictionary<string, string>()
            {
                {"Unknown", "0"},
                {"Ok", "1"},
                {"Nok", "2"},
            };

        public static int[] fanspeed_min_max = { 4000, 7500 };
    }
}

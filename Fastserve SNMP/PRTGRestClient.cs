﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Newtonsoft.Json;
using RestSharp;
using SharedLib;

namespace Fastserve_SNMP
{
    public class PrtgResponse
    {
        public string status { get; set; }
        [JsonProperty("Matching Sensors")]
        public string MatchingSensors { get; set; }
    }

    class PRTGRestClient
    {
        static public RestClient _client = new RestClient("http://10.35.10.41:5050");

        static public string ComposePRTGMessage(SNMPSensor Sensor)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"", "SensorData.xml");
            string PRTGSendData = File.ReadAllText(path);


            XmlDocument doc = new XmlDocument();
            doc.LoadXml(PRTGSendData);
            
            XmlNode TextNode = doc.SelectSingleNode("//text");
            DateTime TreshholdDateTime = DateTime.Now.AddMinutes(-1 * Sensor.alarm_treshhold_minutes);

            // Raise and alarm if sensor state == Nok for more than an hour.
            if (Sensor.state == State.Nok && Sensor.last_ok_state < TreshholdDateTime) // In error state longer than treshhold time
            {
                XmlNode ErrorNode = doc.SelectSingleNode("//error");
                ErrorNode.InnerText = "1";

                TextNode.InnerText = Sensor.action_required;
            }
            else if(Sensor.state == State.Nok && Sensor.last_ok_state > TreshholdDateTime) // In error state but not longer than treshhold time
            {
                XmlNode WarningNode = doc.SelectSingleNode("//result/warning");
                WarningNode.InnerText = "1";
                
                XmlNode ErrorNode = doc.SelectSingleNode("//error");
                ErrorNode.InnerText = "0";

                TextNode.InnerText = Sensor.action_required;
            }
            else // all good
            {
                XmlNode ErrorNode = doc.SelectSingleNode("//error");
                ErrorNode.InnerText = "0";

                TextNode.InnerText = Sensor.description;
            }

            XmlNode ChannelNode = doc.SelectSingleNode("//channel");
            ChannelNode.InnerText = Sensor.description;

            XmlNode ValueNode = doc.SelectSingleNode("//value");
            ValueNode.InnerText = Sensor.rawvalue ?? "";

            return doc.OuterXml;
        }

        static public PrtgResponse SubmitSensor(string SensorID, string Message)
        {
            var request = new RestRequest(SensorID, Method.GET);
            request.AddParameter("content", Message, ParameterType.GetOrPost);
            IRestResponse response = _client.Execute(request);
            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<PrtgResponse>(response.Content);
            else
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"SubmitSensor: {response.ErrorMessage}" );
                return null;
            }
        }
    }
}

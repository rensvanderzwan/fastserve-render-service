﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

// Install the service:
// sc create "Fastserve Manager4" binpath="C:\Source\Repos\Fastserve Render Service\WindowsService Fastserve Manager\bin\Release\netcoreapp2.2\win-x64\Windows Service Fastserve Manager.exe"


namespace WindowsService_Fastserve_Manager
{
      internal class ServiceStarter : ServiceBase
    {
        private Fastserve_Manager.FastServeManagerMain _renderService;

        public ServiceStarter(string StartupArg)
        {
            // InitializeComponent();
            CanShutdown = true;
            _renderService = new Fastserve_Manager.FastServeManagerMain(StartupArg);
        }

        protected override void OnStart(string[] args)
        {
            _renderService.TaskStart();
        }

        protected override void OnShutdown()
        {
            _renderService.TaskShutdown();
            base.OnShutdown();
        }

        protected override void OnStop()
        {
            _renderService.TaskStop();
        }
    }
}

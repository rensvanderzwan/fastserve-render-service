﻿// https://www.pmichaels.net/2019/01/08/creating-a-windows-service-using-net-core-2-2/
// sc create "Fastserve Manager" binpath="C:\Program Files\Fastserve Manager\Windows Service Fastserve Manager.exe"
using System;
using System.ServiceProcess;

namespace WindowsService_Fastserve_Manager
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var service = new ServiceStarter(""))
            {
                ServiceBase.Run(service);
            }
        }
    }
}

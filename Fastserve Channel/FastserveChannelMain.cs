﻿using SharedLib;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;

namespace Fastserve_Channel
{
    class FastserveChannelMain
    {
        private string _hostname = "iets";
        private readonly NameValueCollection _appSettings;
        public FastserveChannelMain(string arg)
        {
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            Logger._LogPath = _appSettings["log_path"];
         //   Logger._rabbitLocation = _appSettings["rabbit_mq.location"];
         //   Logger._rabbitProcessName = _appSettings["rabbit_mq.process_name"];
            Logger._refID = System.Environment.MachineName;
            Logger.Connect();
        }

        public void ConsoleStart()
        {
        //    Logger.WriteConsoleAndLogAndBus(MsgType.INFO, "1", $"############### Start App: {_hostname} ###############");
            Forever();
        }

        public void TaskStart()
        {
            Task.Factory.StartNew(() => Forever());
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Start Service: {_hostname} ###############");
        }

        public void TaskStop()
        {
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Stop Service: {_hostname} ###############");
        }

        public void Forever()
        {
            string[] ChannelList = { "IN 1", "IN 2", "IN 3", "IN 4", "IN 5", "IN 6", "IN 7", "IN 8" };

            List<ChannelStats> ChannelStatsList = new List<ChannelStats>();

            foreach(string Channel in ChannelList)
            {
                ChannelStats CStats = new ChannelStatus().GetSingleChannelStatus("s-heditfast1.edit.nos.nl",Channel);
                ChannelStatsList.Add(CStats);
            }


            List<ChannelStats> StatusList = new ChannelStatus().GetAllChannelStatus("s-heditfast1.edit.nos.nl", ChannelList);

            //while (true)
            //{
      
            //}
        }
    }
}

﻿using Newtonsoft.Json;
using SharedLib;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Fastserve_Manager
{
    public class FastServeManagerMain
    {
        private readonly NameValueCollection _appSettings;
        private readonly int _pollCycle = 60*1000;
        private string _hostname;
        private string _CleanupFolder;
        private int _CleanupAfterXHrs;

        public FastServeManagerMain(string arg)
        {
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            _pollCycle = Convert.ToInt32(_appSettings["poll_cycle_seconds"]) * 1000;

            _CleanupFolder = _appSettings["fastserve_clips"];
            _CleanupAfterXHrs = Convert.ToInt32(_appSettings["cleanup_after_x_hrs"]);

            // Cleanup Fastserve Export folder.
            System.Timers.Timer CleanupTimer = new System.Timers.Timer();
            CleanupTimer.Interval = 120 * 60 * 1000;  // minutes * seconds * milliseconds
            CleanupTimer.Elapsed += (sender, args) => OnTimedEvent(sender, _CleanupFolder, _CleanupAfterXHrs);
            CleanupTimer.Start();
            GC.KeepAlive(CleanupTimer);

            Logger._refID = System.Environment.MachineName;
            Logger.Connect();
            _hostname = System.Environment.MachineName + "." + System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToLower();

        }

        private void OnTimedEvent(object sender, string Folder, int Hours)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(Folder);
                FileInfo[] AllOp1aFiles = di.GetFiles();

                List<FileInfo> OldOp1aFiles = AllOp1aFiles.Where(x => x.LastWriteTime < DateTime.Now.AddHours(-1 * Hours)).ToList();

                foreach (FileInfo OldOp1aFile in OldOp1aFiles)
                {
                    OldOp1aFile.Delete();
                }
                Logger.WriteConsoleAndLogAndBus(MsgType.INFO, _hostname, $"Cleanup Timer: Deleted {OldOp1aFiles.Count()} files older than {Hours} hrs.");
            }
            catch(Exception ex)
            {
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, _hostname, $"Cleanup Timer: {ex.Message}");
            }
        }

        public void ConsoleStart()
        {
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Start App: {_hostname}. Manager ###############");
            Forever();
        }

        public void TaskStart()
        {
            Task.Factory.StartNew(() => Forever());
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Start Service: {_hostname}. Manager ###############");
        }

        public void TaskStop()
        {
            SocketList.ShutConnectionList();
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Stop Service: {_hostname}. Manager ###############");
            DatabaseIO.Close();
        }

        public void TaskShutdown()
        {
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Shutdown Service:{_hostname}. ###############");
            DatabaseIO.Close();
        }

        public void Forever()
        {
            CancellationTokenSource cancelSource = new CancellationTokenSource();
            try
            {
                do
                {
                    Logger.Connect();
                    Logger.RotateLog();
                    InsertClips();
                } while (cancelSource.Token.WaitHandle.WaitOne(_pollCycle) || true);    // wait before diving into the loop again.
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", ex.ToString());
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", $"{ex.Message}");
                DatabaseIO.Close();
                Environment.Exit(1);
            }
        }

        public void InsertClips()
        {
            // Get list of Fastserves for this Fastserve Render Service.
            List<string> ActiveFastserves = DatabaseIO.SelectActiveFastserves();
            if(ActiveFastserves == null)
            {
                Logger.WriteConsoleAndLogAndBus(MsgType.WARN, null, "No Fastserve's to query");
                return;
            }


            // Get newest 20 clips from all Fastserv's
            List<RenderJob> NewestFasserveClips = new RecentClips().GetRecentClips(ActiveFastserves,16);
            if (NewestFasserveClips == null)
            {
                return;
            }

            // Get the newest clips from DB
            List<RenderJob> NewestDBClips = DatabaseIO.SelectNewestJobs(ActiveFastserves,40);
            if (NewestDBClips == null)
            {
                return;
            }

            // Get New Clips
            List<RenderJob> NewClips = NewestFasserveClips.Where(f => !NewestDBClips.Any(t => t.Video_id == f.Video_id)
                                                     ).ToList();

            // Get Changed Clips
            List<RenderJob> ChangedClips = NewestFasserveClips.Where(f => NewestDBClips.Any(t => t.Video_id == f.Video_id &&
                                                                              (t.Length != f.Length ||
                                                                              t.TargetLength != f.TargetLength ||
                                                                              t.Growing != f.Growing))
                                                     ).ToList();

            foreach (RenderJob NewClip in NewClips)
            {
                // since device name is like xxxxx.1a.1 we need to remove two extensions
                string Capture_clip_name = Path.GetFileNameWithoutExtension(NewClip.Device_clip_name);
                NewClip.Capture_clip_name = Path.GetFileNameWithoutExtension(Capture_clip_name);
                DatabaseIO.InsertJob(NewClip);
            }

            foreach (RenderJob ChangedClip in ChangedClips)
            {
                ChangedClip.Job_id = NewestDBClips.FirstOrDefault(clip => clip.Video_id == ChangedClip.Video_id).Job_id;
                DatabaseIO.UpdateJob(ChangedClip);
            }
        }
    }
}

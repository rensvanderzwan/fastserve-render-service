﻿using SharedLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Fastserve_Manager
{
    class RecentClips
    {
        public List<RenderJob> GetRecentClips(List<string> ActiFastserves, int Limit)
        {
            // Get a list of clips from each Fastserve
            // Add the hostname, so after sorting and truncating we know wich id is from wich fastserve

            List<RenderJob> summarizedList = new List<RenderJob>();

            SocketList.BuildConnectionList(ActiFastserves);


            foreach (string Host in ActiFastserves)
            {
                List<RenderJob> SingleList = GetRecentlistFromFastserve(Host);
                SingleList.All(x => { x.Fastserve_host_naam = Host; return true; });
                List<RenderJob> SingleListSorted = SingleList.OrderByDescending(o => o.Clip_creation_date).ToList();
                List<RenderJob> SingleListLimited = SingleListSorted.Take(Limit).ToList();

                summarizedList.AddRange(SingleListLimited);
            }
            return summarizedList;
        }


        private List<RenderJob> GetRecentlistFromFastserve(string FastServeHostName)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"OCIPCommands\", "GetListOfmedia.xml");
            string GetListOfmedia = File.ReadAllText(path);

            socketClient Socketconnection = SocketList._SocketClients.FirstOrDefault(x => x._hostName == FastServeHostName);


            bool SendSucces = Socketconnection.Send(GetListOfmedia, 0, GetListOfmedia.Length, 15000);
            if (SendSucces == false)
            {
                return new List<RenderJob>();
            }

            string Received = Socketconnection.Receive();
            if (Received == "")
            {
                return new List<RenderJob>();
            }

            XmlNodeList ItemList = ValidateAndParseXML(Received);
            if (ItemList == null)
            {
                // try next time if parsing errors occur
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", "GetRecentlistFromFastserve: Error parsing XML from Fastserve");
                return new List<RenderJob>();
            }
            List<RenderJob> ParsedClipsList = ParseItems(ItemList);
            if (ParsedClipsList == null)
            {
                return new List<RenderJob>();
            }
            Console.WriteLine($"{FastServeHostName} {ParsedClipsList.Count}");
            return ParsedClipsList;
        }


        private List<RenderJob> ParseItems(XmlNodeList ItemList)
        {
            List<RenderJob> ClipList = new List<RenderJob>();
            foreach (XmlNode Node in ItemList)
            {
                try
                {
                    string CreationDatestr = Node.SelectSingleNode(".//With[@Parameter='CreationDate']").InnerText;
                    string TapeID = Node.SelectSingleNode(".//Tag[@Name='TapeId']")?.InnerText;
                    string Length = Node.SelectSingleNode(".//With[@Parameter='Length']")?.InnerText.Trim();
                    string TargetLength = Node.SelectSingleNode(".//With[@Parameter='TargetLength']")?.InnerText.Trim();
                    string Growing = Node.SelectSingleNode(".//With[@Parameter='Growing']")?.InnerText;
                    string MasterTCIn = Node.SelectSingleNode(".//With[@Parameter='MasterTCIn']").InnerText.Trim();
                    string MasterTCOut = Node.SelectSingleNode(".//With[@Parameter='MasterTCOut']").InnerText.Trim();
                    string MasterTCOffset = Node.SelectSingleNode(".//With[@Parameter='MasterTCOffset']").InnerText.Trim();
                    string CreationDate = Node.SelectSingleNode(".//With[@Parameter='CreationDate']").InnerText;
                    string InterlacedStr = Node.SelectSingleNode(".//With[@Parameter='Interlaced']").InnerText;
                    bool Interlaced = Convert.ToBoolean(InterlacedStr);
                    string FramerateStr = Node.SelectSingleNode(".//With[@Parameter='Framerate']").InnerText;
                    double Framerate = double.Parse(FramerateStr, System.Globalization.CultureInfo.InvariantCulture);
                    string Name = Node.SelectSingleNode(".//With[@Parameter='Name']").InnerText;
                    Guid ItemID = Guid.Parse(Node.SelectSingleNode("//Item[@Id]/@Id").Value);

                    if (TapeID != null) // not all items are clips, so don't include them
                    {
                        RenderJob Clip = new RenderJob()
                        {
                            ItemID = Guid.Parse(Node.Attributes["Id"]?.Value),
                            Clip_creation_date = DateTime.Parse(CreationDatestr),
                            Video_id = TapeID,
                            Length = Length,
                            TargetLength = TargetLength,
                            Growing = Convert.ToBoolean(Growing),
                            MasterTCIn = MasterTCIn,
                            MasterTCOut = MasterTCOut,
                            MasterTCOffset = MasterTCOffset,
                            Interlaced = Interlaced,
                            Framerate = Framerate,
                            Device_clip_name = Name
                        };
                        ClipList.Add(Clip);
                    }
                }
                catch(Exception ex)
                {
                    Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Unable to create a recentclip: {ex.Message}");
                    return null;
                }
            }
            return ClipList;
        }


        static XmlNodeList ValidateAndParseXML(string Xml)
        {
            // document has multiple root Nodes named "OCIP", so add a single root node to avoid parsing errors
            Xml = "<root>" + Xml + "</root>";
            XmlElement Root;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(Xml);
                Root = doc.DocumentElement;
            }
            catch (XmlException ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", "Error loading XML document: " + ex.Message);
                return null;
            }

            // The returnvalue of the Acknowledge Attribute should be "OK"
            string Acknowledge = Root.SelectSingleNode("//OCIP/Ack")?.Attributes["Return"]?.Value;
            if (Acknowledge != "OK")
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Acknowledge result is: {Acknowledge}");
                return null;
            }

            // The returnvalue of the Result Attribute should be "OK"
            string Result = Root.SelectSingleNode("//OCIP/Result")?.Attributes["Return"]?.Value;
            if (Result != "OK")
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Result is: {Result}");
                return null;
            }

            // The returnvalue of the Finished Attribute should be "OK"
            string Finished = Root.SelectSingleNode("//OCIP/Finished")?.Attributes["Return"]?.Value;
            if (Finished != "OK")
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Finished result is: {Finished}");
                return null;
            }

            // if the acknowledge and finished response is Ok than we should have 0 or more items in the Result
            XmlNodeList Items = Root.SelectNodes("//OCIP/Result/Item");

            return Items;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fastserve_Service_Starter_Service
{
    public class ServiceStartAndStop
    {
        public string _mode;
        public string[] _serviceNames;
        public ServiceStartAndStop(string arg)
        {
            _mode = arg;
            _serviceNames = new string[]
            {
                //"Fastserve Manager",
                "Fastserve Worker 1",
                "Fastserve Worker 2",
                "Fastserve Worker 3",
                "Fastserve Worker 4",
                "Fastserve Worker 5",
                "Fastserve Worker 6",
                "Fastserve Worker 7",
                "Fastserve Worker 8",
                "Fastserve Worker 9",
                "Fastserve Worker 10",
                "Fastserve Worker 11",
                "Fastserve Worker 12",
            };
        }


        public void ConsoleStart()
        {
            Start();
        }

        public void TaskStart()
        {
            Task.Factory.StartNew(() => Start());
        }

        public void TaskStop()
        {
        }

        public void Start()
        {
            switch (_mode.ToLower())
            {
                case "start":
                    foreach (var service in _serviceNames)
                    {
                        Thread.Sleep(15000);
                        if (ServiceStartAndStop.DoesServiceExist(service))
                            ServiceStartAndStop.StartService(service, 60000);
                    }
                    ServiceStartAndStop.StopService("Fastserve Services Start", 60000);
                    break;
                case "stop":
                    foreach (var service in _serviceNames)
                    {
                        if (ServiceStartAndStop.DoesServiceExist(service))
                            ServiceStartAndStop.StopService(service, 60000);
                    }
                    ServiceStartAndStop.StopService("Fastserve Services Stop", 60000);
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }


        public static bool DoesServiceExist(string serviceName, string machineName = ".")
        {
            ServiceController[] services = ServiceController.GetServices(machineName);
            var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
            return service != null;
        }

        public static void StopService(string serviceName, int timeoutMilliseconds)
        {
            using (ServiceController service = new ServiceController(serviceName))
            {
                // Do not start the service if it is already running
                if (service.Status == ServiceControllerStatus.Stopped)
                    return;

                try
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                    Console.WriteLine($"The Windows Service '{serviceName}' has been successfully stopped");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Could not stop " + serviceName + " Service.\n Error: " + ex.Message.ToString());
                }
            }
        }

        public static void StartService(string serviceName, int timeoutMilliseconds)
        {
            using (ServiceController service = new ServiceController(serviceName))
            {
                // Do not start the service if it is already running
                if (service.Status == ServiceControllerStatus.Running)
                    return;

                try
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);

                    Console.WriteLine($"The Windows Service '{serviceName}' has been successfully started");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Could not start " + serviceName + " Service.\n Error: " + ex.Message.ToString());
                }
            }
        }
    }
}

﻿using System;

namespace Fastserve_Service_Starter_Service
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = new ServiceStartAndStop(args[0]);
            result.ConsoleStart();
        }
    }
}

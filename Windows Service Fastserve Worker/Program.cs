﻿using System;
using System.ServiceProcess;
// sc create "Fastserve Worker 1" binpath="C:\Program Files\Fastserve Worker\Windows Service Fastserve Worker.exe 1"


namespace Windows_Service_Fastserve_Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var service = new ServiceStarter(args[0]))
            {
                ServiceBase.Run(service);
            }
        }
    }
}

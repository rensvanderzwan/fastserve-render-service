﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

// Install service:
// sc create "Fastserve Worker 1" binpath= "C:\Source\Repos\Fastserve Render Service\Windows Service Fastserve Worker\bin\Release\netcoreapp2.2\win-x64\Windows Service Fastserve Worker.exe 1"

namespace Windows_Service_Fastserve_Worker
{
    internal class ServiceStarter : ServiceBase
    {
        private Fastserve_Worker.FastserveWorkerMain _renderService;

        public ServiceStarter(string StartupArg)
        {
            // InitializeComponent();
            CanShutdown = true;
            _renderService = new Fastserve_Worker.FastserveWorkerMain(StartupArg);
        }

        protected override void OnStart(string[] args)
        {
            _renderService.TaskStart();
        }

        protected override void OnShutdown()
        {
            _renderService.TaskShutdown();
            base.OnShutdown();
        }

        protected override void OnStop()
        {
            _renderService.TaskStop();
        }
    }
}

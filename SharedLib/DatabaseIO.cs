﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace SharedLib
{
    static public class DatabaseIO
    {
        private static NameValueCollection _appSettings;
        static private string _DatabaseHost;
        static private string _MyConnString;
        static private MySqlConnection _Connection;

        static DatabaseIO()
        {
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            _DatabaseHost = _appSettings["db_host"];

            _MyConnString = $"server={_DatabaseHost};uid=fastserve_svc;pwd=8ChannelsIngest;database=fastserve_render_service;useAffectedRows=true;SslMode=None;pooling=false";
        }

        static private bool Open()
        {
            if (_Connection != null && _Connection.State == System.Data.ConnectionState.Open) return true;

            try
            {
                _Connection = new MySqlConnection(_MyConnString);
                _Connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Open Mysql connection1: {ex.Message}");
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Open Mysql connection2: {ex.Message}");
                return false;
            }
        }

        static public void Close()
        {
            if (_Connection != null && _Connection.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    _Connection.Close();
                    _Connection.Dispose();
                    _Connection.ClearPoolAsync(_Connection);
                }
                catch (Exception)
                {
                    // No need to log, where terminating anyway
                }
            }
        }

        static public RenderJob GetNewJob()
        {
            // select jobs older than 2 minutes to avoid rendering to far in a Growing files.
            Guid id = Guid.NewGuid();

            string UpdateQuery = @"UPDATE jobs 
                                  SET guid = @id 
                                  WHERE
	                                  job_status is NULL
	                                  AND guid IS NULL 
	                                  AND job_creation_date < NOW( ) - INTERVAL 1 MINUTE 
                                  ORDER BY
	                                  job_creation_date DESC 
	                                  LIMIT 1";

            string SelectQuery = @"SELECT 
                                        * 
                                     FROM 
                                        jobs
                                     WHERE guid = @id ";


            using (_Connection)
            {
                if (Open() == false) return null;

                using (var sc = new MySqlCommand(UpdateQuery, _Connection))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@id", id);
                        int Updated = sc.ExecuteNonQuery();   // 1 = new / 2 = update / 0 = not changed
                        if (Updated == 0) return null;  // noting to do
                    }
                    catch(MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Unable to set guid: {ErrorMessage}");
                        return null;
                    }
                    catch (NullReferenceException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to set guid: {ErrorMessage}");
                        return null;
                    }
                }


                using (var sc = new MySqlCommand(SelectQuery, _Connection))
                {
                    sc.Parameters.AddWithValue("@id", id);
                    try
                    {
                        using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                        {

                            RenderJob Job = new RenderJob();
                            while (mySQlReader.Read())
                            {
                                Job.Job_id = Convert.ToInt32(mySQlReader["job_id"]);
                                Job.ItemID = Guid.Parse(mySQlReader.GetString("item_id"));
                                Job.Job_creation_date = Convert.ToDateTime(mySQlReader["job_creation_date"]);
                                Job.Clip_creation_date = Convert.ToDateTime(mySQlReader["clip_creation_date"]);
                                Job.Fastserve_host_naam = mySQlReader.GetString("fastserve_host_naam");
                                Job.Device_clip_name = mySQlReader.GetString("device_clip_name");
                                Job.Capture_clip_name = mySQlReader.GetString("capture_clip_name");
                                Job.Video_id = mySQlReader.GetString("video_id");
                                Job.Framerate = Convert.ToDouble(mySQlReader["frame_rate"]);
                                Job.Interlaced = Convert.ToBoolean(mySQlReader["interlaced"]);
                                Job.Length = mySQlReader.GetString("length");
                                Job.MasterTCOffset = mySQlReader.GetString("master_tc_offset");
                                Job.MasterTCIn = mySQlReader.GetString("master_tc_in");
                                Job.MasterTCOut = mySQlReader.GetString("master_tc_out");
                                Job.Growing = Convert.ToBoolean(mySQlReader["Growing"]);
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("ingest_service_checkin")))
                                    Job.Ingest_service_checkin = Convert.ToBoolean(mySQlReader["ingest_service_checkin"]);
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("target_length")))
                                    Job.TargetLength = mySQlReader["target_length"].ToString();
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("job_error_description")))
                                    Job.Job_error_description = mySQlReader["job_error_description"].ToString();
                            }
                            return Job;

                        }
                    }
                    catch (MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Unable to get newjob: {ErrorMessage}");
                        return null;
                    }
                    catch (NullReferenceException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Unable to get newjob: {ErrorMessage}");
                        return null;
                    }
                    catch (Exception ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Unable to get newjob: {ErrorMessage}");
                        return null;
                    }
                }
            }
        }


        static public long UpdateJob(RenderJob Job)
        {
            string Query = @"UPDATE jobs SET 
                                device_clip_name = COALESCE(@device_clip_name, device_clip_name),       
                                frame_rate = COALESCE(@frame_rate, frame_rate),
                                interlaced = COALESCE(@interlaced, interlaced),
                                length = COALESCE(@length, length),
                                job_start = COALESCE(@job_start, job_start),
                                job_end = COALESCE(@job_end, job_end),
                                master_tc_offset = COALESCE(@master_tc_offset, master_tc_offset),
                                master_tc_in = COALESCE(@master_tc_in, master_tc_in),
                                master_tc_out = COALESCE(@master_tc_out, master_tc_out),
                                render_host_name = COALESCE(@render_host_name, render_host_name),
                                growing = COALESCE(@growing, growing),
                                job_status = COALESCE(@job_status, job_status),
                                job_progress = COALESCE(@job_progress, job_progress),
                                target_length = COALESCE(@target_length, target_length),
                                worker_id = COALESCE(@worker_id, worker_id),
                                ingest_service_checkin = COALESCE(@ingest_service_checkin, ingest_service_checkin),
                                job_error_description = IF(ISnull(job_error_description),@job_error_description,CONCAT_WS('\r\n',job_error_description,@job_error_description))
                             WHERE job_id = @job_id";

            using (_Connection)
            {
                if (Open() == false) return 0;

                using (var sc = new MySqlCommand(Query, _Connection))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@job_id", Job.Job_id);
                        sc.Parameters.AddWithValue("@device_clip_name", Job.Device_clip_name);
                        sc.Parameters.AddWithValue("@frame_rate", Job.Framerate);
                        sc.Parameters.AddWithValue("@interlaced", Job.Interlaced);
                        sc.Parameters.AddWithValue("@length", Job.Length);
                        sc.Parameters.AddWithValue("@job_start", Job.Job_start);
                        sc.Parameters.AddWithValue("@job_end", Job.Job_end);
                        sc.Parameters.AddWithValue("@master_tc_offset", Job.MasterTCOffset);
                        sc.Parameters.AddWithValue("@master_tc_in", Job.MasterTCIn);
                        sc.Parameters.AddWithValue("@master_tc_out", Job.MasterTCOut);
                        sc.Parameters.AddWithValue("@growing", Job.Growing);
                        sc.Parameters.AddWithValue("@job_status", Job.Job_status);
                        sc.Parameters.AddWithValue("@job_error_description", Job.Job_error_description);
                        sc.Parameters.AddWithValue("@job_progress", Job.Job_progress);
                        sc.Parameters.AddWithValue("@render_host_name", Job.Render_host_name);
                        sc.Parameters.AddWithValue("@target_length", Job.TargetLength);
                        sc.Parameters.AddWithValue("@worker_id", Job.Worker_id);
                        sc.Parameters.AddWithValue("@ingest_service_checkin", Job.Ingest_service_checkin);
                        int Updated = sc.ExecuteNonQuery();   // 1 = new / 2 = update / 0 = not changed
                        return Updated;
                    }
                    catch(MySqlException ex)
                    {
                        var ErrorMessage = $"Message: {ex.Message} InnerException1: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"UpdateJob1: {Job.Job_id} {ErrorMessage}", JsonConvert.SerializeObject(Job));
                        return 0;
                    }
                    catch(NullReferenceException ex)
                    {
                        // seen a nullreference exception once or twice during updates by TCPlistener
                        //
                        var ErrorMessage = $"Message: {ex.Message} InnerException2: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"UpdateJob2: {Job.Job_id} {ErrorMessage}", JsonConvert.SerializeObject(Job));
                        return 0;
                    }
                    catch (AggregateException ex)
                    {
                        // seen a nullreference exception once or twice during updates by TCPlistener
                        //
                        var ErrorMessage = $"Message: {ex.Message} InnerException2: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"UpdateJob3: {Job.Job_id} {ErrorMessage}", JsonConvert.SerializeObject(Job));
                        return 0;
                    }
                    catch (Exception ex)
                    {
                        // Added a final exception in case the above does not catch the nullreference exception.
                        var ErrorMessage = $"Message: {ex.Message} InnerException3: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"UpdateJob4: {Job.Job_id} {ErrorMessage}", JsonConvert.SerializeObject(Job));
                        return 0;
                    }
                }
            }
        }


        static public List<string> SelectActiveFastserves()
        {
            string Query = "SELECT * FROM fastserve_hosts where active = 1;";

            using (_Connection)
            {
                if (Open() == false) return null;

                using (var sc = new MySqlCommand(Query, _Connection))
                {
                    try
                    {
                        using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                        {
                            List<string> FSHostList = new List<string>();
                            while (mySQlReader.Read())
                            {
                                FSHostList.Add(mySQlReader["fastserve_host_name"].ToString());
                            }
                            return FSHostList;
                        }
                    }
                    catch(MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "SelectActiveFastserves: " + ErrorMessage);
                        return null;
                    }
                }
            }
        }


        static public List<RenderJob> SelectNewestJobs(List<string> ActiFastserves, int Limit)
        {

            // get the newest limit jobs from each fastserve
            List<string> querylist =  new List<string>();
            foreach(string host in ActiFastserves)
            {
                querylist.Add($"(select * from jobs where fastserve_host_naam = '{host}' ORDER BY clip_creation_date DESC limit @limit)");
            }
            string Query = String.Join(" UNION ", querylist.ToArray());


            using (_Connection)
            {
                if (Open() == false) return null;

                using (var sc = new MySqlCommand(Query, _Connection))
                {
                    sc.Parameters.AddWithValue("@limit", Limit);
                    try
                    {
                        using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                        {
                            List<RenderJob> RecentClips = new List<RenderJob>();
                            while (mySQlReader.Read())
                            {
                                RenderJob Clip = new RenderJob()
                                {
                                    Job_id = Convert.ToInt64(mySQlReader["job_id"]),
                                    ItemID = Guid.Parse(mySQlReader["item_id"].ToString()),
                                    Video_id = mySQlReader["video_id"].ToString(),
                                    Length = mySQlReader["length"].ToString(),
                                    TargetLength = mySQlReader["target_length"].ToString(),
                                    Growing = Convert.ToBoolean(mySQlReader["growing"])
                                };
                                RecentClips.Add(Clip);
                            }
                            return RecentClips;
                        }
                    }
                    catch(MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "SelectNewestJobs: " + ErrorMessage);
                        return null;
                    }
                }
            }
        }

        static public void InsertJob(RenderJob Job)
        {
            // When multiple Managers are running there is a small risk the same ingest
            // is inserted multiple times because there is a time delay between getting clips from
            // Fastserve and insertion. Therefore ignore a duplicate key by setting job_id
            // to itself so no exception is generated and nothing is changed.

            string Query = @"INSERT INTO jobs ( 
                                                job_creation_date,
                                                clip_creation_date,
                                                fastserve_host_naam,
                                                item_id,
                                                job_status,
                                                device_clip_name,
                                                capture_clip_name,
                                                frame_rate,
                                                interlaced,
                                                growing,
                                                length,
                                                master_tc_offset,
                                                master_tc_in,
                                                master_tc_out,
                                                video_id,
                                                target_length
                                              ) VALUES ( 
                                                @job_creation_date,
                                                @clip_creation_date,
                                                @fastserve_host_naam,
                                                @item_id,
                                                @job_status,
                                                @device_clip_name,
                                                @capture_clip_name,
                                                @frame_rate,
                                                @interlaced,
                                                @growing,
                                                @length,
                                                @master_tc_offset,
                                                @master_tc_in,
                                                @master_tc_out,
                                                @video_id,
                                                @target_length
                                              ) ON DUPLICATE KEY 
                                                update Job_id = job_id;";


            using (_Connection)
            {
                if (Open() == false) return;

                using (var sc = new MySqlCommand(Query, _Connection))
                {
                    try
                    {
                        Job.Job_creation_date = DateTime.Now;
                        sc.Parameters.AddWithValue("@job_creation_date", Job.Job_creation_date);
                        sc.Parameters.AddWithValue("@clip_creation_date", Job.Clip_creation_date);
                        sc.Parameters.AddWithValue("@fastserve_host_naam", Job.Fastserve_host_naam);
                        sc.Parameters.AddWithValue("@item_id", Job.ItemID.ToString());
                        sc.Parameters.AddWithValue("@job_status", Job.Job_status);
                        sc.Parameters.AddWithValue("@device_clip_name", Job.Device_clip_name);
                        sc.Parameters.AddWithValue("@capture_clip_name", Job.Capture_clip_name);
                        sc.Parameters.AddWithValue("@frame_rate", Job.Framerate);
                        sc.Parameters.AddWithValue("@interlaced", Job.Interlaced);
                        sc.Parameters.AddWithValue("@growing", Job.Growing);
                        sc.Parameters.AddWithValue("@length", Job.Length);
                        sc.Parameters.AddWithValue("@master_tc_offset", Job.MasterTCOffset);
                        sc.Parameters.AddWithValue("@master_tc_in", Job.MasterTCIn);
                        sc.Parameters.AddWithValue("@master_tc_out", Job.MasterTCOut);
                        sc.Parameters.AddWithValue("@video_id", Job.Video_id);
                        sc.Parameters.AddWithValue("@target_length", Job.TargetLength);

                        if (sc.ExecuteNonQuery() == 1)      // 1 = new / 2 = update / 0 = not changed
                        {
                            Job.Job_id = sc.LastInsertedId;
                            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, Job.Job_id.ToString(), $"Found a new clip {Job.Capture_clip_name} on Fastserve {Job.Fastserve_host_naam}", JsonConvert.SerializeObject(Job, Formatting.Indented));
                        }                                                    
                        return;
                    }
                    catch (MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "InsertJob: " + ErrorMessage);
                        return;
                    }
                }
            }
        }



        static public RenderJob GetJob(long Id)
        {
            string Query = "SELECT * FROM jobs where job_id = @Id;";

            using (_Connection)
            {
                if (Open() == false) return null;

                using (var sc = new MySqlCommand(Query, _Connection))
                {
                    sc.Parameters.AddWithValue("@Id", Id);
                    try
                    {
                        using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                        {

                            RenderJob Clip = new RenderJob();
                            while (mySQlReader.Read())
                            {
                                {
                                    Clip.Job_id = Convert.ToInt64(mySQlReader["job_id"]);
                                    Clip.Length = mySQlReader["length"].ToString();
                                    Clip.TargetLength = mySQlReader["target_length"].ToString();
                                    Clip.Growing = Convert.ToBoolean(mySQlReader["growing"]);
                                };
                            }
                            return Clip;
                        }
                    }
                    catch (MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "GetJob1: " + ErrorMessage);
                        return null;
                    }
                    catch (AggregateException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "GetJob2: " + ErrorMessage);
                        return null;
                    }
                    catch (Exception ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "GetJob3: " + ErrorMessage);
                        return null;
                    }
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace SharedLib
{
    
    public class RenderJob
    {
        public long Job_id { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime Job_creation_date { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime Clip_creation_date { get; set; }
        public string Fastserve_host_naam { get; set; }
        public string Video_id { get; set; }  // Is the same as fastserve's tapeid
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime? Job_start { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime? Job_end { get; set; }
        public string Job_error_description { get; set; }
        public string Render_host_name { get; set; }
        public int? Worker_id { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Status? Job_status { get; set; }
        public int? Job_progress { get; set; }
        public string Export_location { get; set; }
        public bool? Ingest_service_checkin { get; set; }
        public Guid ItemID { get; set; }
        public string Device_clip_name { get; set; }
        public string Capture_clip_name { get; set; }
        public double? Framerate { get; set; }
        public bool? Interlaced { get; set; }
        public bool? Growing { get; set; }
        public string TargetLength { get; set; }
        public string Length { get; set; }
        public string MasterTCOffset { get; set; }
        public string MasterTCIn { get; set; }
        public string MasterTCOut { get; set; }
    }

    public enum Status
    {
        New,
        Active,
        Finished,
        Error
    }

    class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SharedLib
{

    static public class SocketList
    {
        // This is where we keep a static list of connections to all the fastserve's in the list
        static public List<socketClient> _SocketClients = new List<socketClient>();
        static List<string> _Hosts = new List<string>();

        static public void BuildConnectionList(List<string> Hosts)
        {
            // Make a socket connection to all fastserve's
            _Hosts = Hosts;
            foreach (string Host in Hosts)
            {
                // if host not in _SocketClients create connection and add to the list
                if (!_SocketClients.Any(str => str._hostName == Host))
                {
                    socketClient Client = new socketClient();
                    Client.InitSocket(Host);
                    _SocketClients.Add(Client);
                }
            }


            // if _SocketClients contains hosts not in Hosts remove them
            foreach (socketClient SockClient in _SocketClients.ToList())
            {
                if (!Hosts.Any(str => str == SockClient._hostName))
                {
                    _SocketClients.Remove(SockClient);
                }
            }
        }

        static public void ShutConnectionList()
        {
            // Disconnect from all fastserve's
            foreach (socketClient Client in _SocketClients)
            {
                Client.CloseSocket();
            }
        }
    }
    public class socketClient
    {
        private Socket _fSSocket;
        private TcpClient _tcpClient;
        private int _port = 10011;
        public string _hostName;

        public bool InitSocket(string FastServeHostName)
        {
            if (_fSSocket != null && _fSSocket.Connected == true) return true;

            if (_fSSocket != null && _fSSocket.Connected == false) Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"No TCP connection for {FastServeHostName}, Was connection lost?");
            try
            {
                _hostName = FastServeHostName;
                _tcpClient = new TcpClient(FastServeHostName, _port);
                _fSSocket = _tcpClient.Client;
                _fSSocket.SendTimeout = 15000;
                _fSSocket.ReceiveTimeout = 15000;
                return true;
            }
            catch (SocketException ex)
            {
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"TCPClient error for {FastServeHostName}: {ex.Message}");
                return false;
            }
        }

        public void CloseSocket()
        {
            try
            {
                _tcpClient.Close();
            }
            catch (Exception ex)
            {

            }
            try
            {
                _fSSocket.Close();
                _fSSocket.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        public bool Send(string Command, int offset, int size, int timeout)
        {
            // Test if we still have a socket connection to this specific Fastserve.
            // If not reconnect and if reconnection fails return false
            if (InitSocket(_hostName) == false)
            {
                return false;
            }


            byte[] buffer = Encoding.UTF8.GetBytes(Command);
            int startTickCount = Environment.TickCount;
            int sent = 0;  // how many bytes is already sent
            do
            {
                if (Environment.TickCount > startTickCount + timeout)
                {
                    Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Timeout while sending:");
                    this.CloseSocket();
                    return false;  // any serious error occurr}
                }

                try
                {
                    sent += _fSSocket.Send(buffer, offset + sent, size - sent, SocketFlags.None);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.WouldBlock ||
                        ex.SocketErrorCode == SocketError.IOPending ||
                        ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                    {
                        // socket buffer is probably full, wait and try again
                        Thread.Sleep(30);
                    }
                    else
                    {
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Error while sending: {ex.Message}");
                        this.CloseSocket();
                        return false;  // any serious error occurr}
                    }

                }
            } while (sent < size);
            return true;
        }



        public string Receive()
        {
            StringBuilder builder = new StringBuilder();

            while (!builder.ToString().Contains("><Finished Return=\"OK\"/></OCIP>") && !builder.ToString().Contains("><Finished Return=\"Fail\"/></OCIP>"))
            {
                try
                {
                    var currByte = new Byte[65335];
                    var byteCounter = _fSSocket.Receive(currByte, currByte.Length, SocketFlags.None);

                    if (byteCounter > 0)
                    {
                        string s = Encoding.ASCII.GetString(currByte, 0, byteCounter);
                        builder.Append(s);
                    }
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.WouldBlock ||
                       ex.SocketErrorCode == SocketError.IOPending ||
                       ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                    {
                        // socket buffer is probably full, wait and try again
                        Thread.Sleep(10);
                    }
                    else
                    {
                        this.CloseSocket();
                        Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Error while receiving: {ex.Message}");
                        return "";
                    }
                }
            }
            return builder.ToString();
        }
    }
}

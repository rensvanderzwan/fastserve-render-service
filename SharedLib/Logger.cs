﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading;

namespace SharedLib
{
    public class RabbitLogMsg
    {
        public string MsgType { get; set; }
        public string Location { get; set; }
        public string ProcessName { get; set; }
        public string RefId { get; set; }
        public string Msg { get; set; }
        public string MsgDetail { get; set; }
        public long Timestamp { get; set; }
    }

    public enum MsgType
    {
        INFO,
        WARN,
        ERROR,
        DEBUG
    }

    public static class Logger
    {
        readonly private static ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();
        public static string _LogPath { get; set; }
        public static string _AvidEnvironment;
        public static IConnection Connection;
        public static IModel Channel;
        public static string _HostName;
        public static string _VirtualHost;
        public static string _UserName;
        public static string _Password;
        public static string _Exchange;
        public static string _RoutingKey;
        public static string _ProcessName;
        public static string _Location;
        public static string _AppId = "Bronnen Service";
        private static NameValueCollection _appSettings;
        public static string _refID = "1";
        public static string _worker = "1";

        public static bool Connect()
        {
            if (Connection != null && Connection.IsOpen && Channel.IsOpen) return true;

            _appSettings = ConfigurationManager.AppSettings;
            _LogPath =  _appSettings["log_path"];
            _HostName = _appSettings["rabbit_mq.host_name"];
            _VirtualHost = _appSettings["rabbit_mq.virtual_host"];
            _UserName = _appSettings["rabbit_mq.user_name"];
            _Password = _appSettings["rabbit_mq.password"];
            _AppId = _appSettings["rabbit_mq.app_id"];
            _Exchange =_appSettings["rabbit_mq.log_exchange"];
            _RoutingKey = _appSettings["rabbit_mq.log_routing_key"];
            _ProcessName =  _appSettings["rabbit_mq.process_name"];
            _Location =  _appSettings["rabbit_mq.location"];

            var factory = new ConnectionFactory() { HostName = _HostName, VirtualHost = _VirtualHost, UserName = _UserName, Password = _Password };
            factory.AutomaticRecoveryEnabled = true;
            //factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);


            try
            {
                Connection = factory.CreateConnection();
                Channel = Connection.CreateModel();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"RabbitConnector: {ex.Message}, {ex.InnerException}");
                return false;
            }
        }


        public static void Disconnect()
        {
            try
            {
                if (Channel != null && Channel.IsOpen)
                {
                    Channel.Close();
                    Channel = null;
                }

                if (Connection != null && Connection.IsOpen)
                {
                    Connection.Close();
                    Connection = null;
                }
            }
            catch (IOException)
            {
                // Close() may throw an IOException if connection dies - but that's ok
            }
        }



        public static IBasicProperties CreateBasicProperties(string Type)
        {
            var properties = Channel.CreateBasicProperties();
            properties.AppId = _AppId;
            properties.Type = Type;
            properties.MessageId = "12345";
            properties.ReplyTo = "";
            properties.CorrelationId = "";
            Dictionary<string, object> Headers = new Dictionary<string, object>();
            Headers.Add("time", DateTime.UtcNow.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ"));
            properties.Headers = Headers;

            return properties;
        }



        public static void RotateLog()
        {
            if (File.Exists(_LogPath))
            {
                FileInfo f = new FileInfo(_LogPath);
                if (f.Length > 5 * 1024 * 1024)
                {
                    string ArchiveName = _LogPath + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".log";
                    File.Delete(ArchiveName); // Delete the existing file if exists
                    File.Move(_LogPath, ArchiveName);
                    WriteLog(MsgType.INFO, "--", $"Log Rotation: {ArchiveName}");
                }
            }
        }


        public static void WriteLog(MsgType LogType, string refID, string Message)
        {
            string Reference = refID ?? _refID ?? _worker ?? "1";
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_LogPath));
            try
            {
                using (StreamWriter w = File.AppendText(_LogPath))
                {
                    w.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {Reference.PadLeft(6, ' ')} {LogType.ToString()}: {Message}");
                    w.Close();
                }
            }
            catch (Exception)
            {
                // How to log this exeception?
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }

        public static void WriteConsoleAndLog(MsgType LogType, string refID, string Message)
        {
            string Reference = refID ?? _refID ?? _worker ?? "1";
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_LogPath));
            try
            {
                using (StreamWriter w = File.AppendText(_LogPath))
                {
                    w.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {Reference.PadLeft(6, ' ')} {LogType.ToString()}: {Message}");
              //      #if DEBUG
                    Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {Reference.PadLeft(6, ' ')} {Message}");
               //     #endif
                }
            }
            catch (Exception)
            {
                // How to log this exeception?
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }

        public static void WriteConsoleAndLogAndBus(MsgType LogType, string refID, string Message, string DetailMessage = "")
        {
            string Reference = refID ?? _refID ?? _worker ?? "1";
            WriteConsoleAndLog(LogType, Reference, Message);

            RabbitLogMsg msg = new RabbitLogMsg
            {
                MsgType = LogType.ToString(),
                Location = _Location,
                ProcessName = _ProcessName,
                RefId = Reference,
                Msg = Message.Substring(0, Math.Min(Math.Abs(511), Message.Length)),
                MsgDetail = DetailMessage,
                Timestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds * 1000
            };

            string message = JsonConvert.SerializeObject(msg);
            var body = Encoding.UTF8.GetBytes(message);

            try
            {
                Channel.BasicPublish(exchange: _Exchange,
                                     routingKey: _RoutingKey,
                                     mandatory: false,
                                     basicProperties: CreateBasicProperties("logging"),
                                     body: body);
            }
            catch (Exception ex)
            {
                WriteConsoleAndLog(MsgType.ERROR, "", $"Error LogtoBus: {ex.Message}");
                // No need to reconnect here, Is done autmatically for the next Job.
            }
        }
    }
}

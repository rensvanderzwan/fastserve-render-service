﻿using System;

namespace Fastserve_Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            var FastserveWorker = new FastserveWorkerMain(args[0]);
            FastserveWorker.ConsoleStart();
        }
    }
}

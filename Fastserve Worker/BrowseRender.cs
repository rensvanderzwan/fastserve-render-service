﻿using Newtonsoft.Json;
using SharedLib;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace Fastserve_Worker
{
    public class LiveLenght
    {
        public int NewLenght { get; set; }
    }
    class BrowseRender
    {
        private readonly NameValueCollection _appSettings;
        public LiveLenght _newlenghtObj;
        private int _timeout = 300;
        private string _ingest_service_endpoint;
        private string _fastserve_clips;
        public static Mutex mut = new Mutex();  // Use mutex cause threads 

        public BrowseRender(RenderJob Job)
        {
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            _ingest_service_endpoint = _appSettings[$"ingest_service_endpoint"];
            _fastserve_clips = _appSettings[$"fastserve_clips"];


            if (!GetAvailablePort(out int FreeIPPort, Job))
            {
                return;
            }

            string RenderArgs = ComposeRenderArgs(FreeIPPort, Job);
        
            int Lenght = 1;
            if (string.IsNullOrEmpty(Job.TargetLength))
            {
                Lenght = TCFramesConverter.TCToSeconds(Job.Length);
            }
            else
            {
                Lenght = TCFramesConverter.TCToSeconds(Job.TargetLength);
            }

            // wrap the duration in a Object and pass it to the TCPListener.
            // If the duration of the recording is changed than the listener will calculate with the new duration.
            _newlenghtObj = new LiveLenght() { NewLenght = Lenght };
            Job.Job_status = Status.Active;
            Job.Job_start = DateTime.Now;
            Job.Job_progress = 0;
            DatabaseIO.UpdateJob(new RenderJob() { Job_id = Job.Job_id, Job_status = Job.Job_status, Job_start = Job.Job_start, Job_progress = Job.Job_progress });
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO ,null , $"Movie render Start: {Job.Capture_clip_name}", JsonConvert.SerializeObject(Job, Formatting.Indented));


            Task progressTask = new Task(() => new TCPListener(FreeIPPort, Job, _newlenghtObj));
            progressTask.Start();
            IShellProcess SProcess = null;
            var Rendertask = Task.Factory.StartNew(() => { SProcess = new ShellProcess("ffmpeg.exe", RenderArgs, _timeout, ConsoleCtrlEvent.CTRL_C, true); });


            // Report to the Workflow Service every increase in progress as long as the Rendertask runs.
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            CancellationToken ProgressToken = tokenSource.Token;
            Task ProgressTask = Task.Factory.StartNew(() => { IngestProgressAndUpdateRenderJob(Job, ProgressToken); }, ProgressToken);


            Rendertask.Wait();
            progressTask.Wait();
            tokenSource.Cancel();
            ProgressTask.Wait();


            // Wrapup this Job
            ProcessRenderResults(SProcess, Job);

            // Do a final ingestservice check-in now the status is known
            bool result = ProgressToIngestservice(Job);
            Job.Ingest_service_checkin = result;
            DatabaseIO.UpdateJob(new RenderJob() { Job_id = Job.Job_id, Ingest_service_checkin = Job.Ingest_service_checkin });
            Logger.WriteConsoleAndLogAndBus(result ? MsgType.INFO : MsgType.ERROR, null, $"Ingest Service checkin: {Job.Capture_clip_name}", JsonConvert.SerializeObject(Job, Formatting.Indented));
        }


        private void ProcessRenderResults(IShellProcess SProcess, RenderJob Job)
        {
            if (SProcess == null)
            {
                return;
            }
            if (SProcess.TimeOut == true)
            {
                Job.Job_error_description = $"Time out: The Job ran for more than {_timeout} minutes. The Render process was gracfully exited";
                Job.Job_status = Status.Error;
                Job.Job_end = DateTime.Now;
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, Job.Job_id.ToString(), $"Time out: The Job ran for more than {_timeout} minutes. The Render process was gracfully exited", JsonConvert.SerializeObject(Job, Formatting.Indented));
            }

            if (SProcess.ExitCode == 0)
            {
                Job.Job_status = Status.Finished;
                Job.Job_end = DateTime.Now;
                Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"Movie render Finished: {Job.Capture_clip_name}", JsonConvert.SerializeObject(Job, Formatting.Indented));
            }
            else
            {
                Job.Job_error_description = SProcess.Error.ToString();
                Job.Job_end = DateTime.Now;
                Job.Job_status = Status.Error;
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Exitcode not 0: {SProcess.Error.ToString()}", JsonConvert.SerializeObject(Job, Formatting.Indented));
            }
            if (!string.IsNullOrEmpty(SProcess.Error.ToString()))
            {
                Job.Job_error_description = $"Movie render of {Job.Job_id} Error: {SProcess.Error.ToString().Trim()}"; ;
                Logger.WriteConsoleAndLogAndBus(MsgType.WARN, null, $"Error Message: {SProcess.Error.ToString()}", JsonConvert.SerializeObject(Job, Formatting.Indented));
            }
            DatabaseIO.UpdateJob(new RenderJob() { Job_id = Job.Job_id, Job_status = Job.Job_status, Job_error_description = Job.Job_error_description, Job_end = Job.Job_end });
        }

        private string ComposeRenderArgs(int FreeIPPort, RenderJob Job)
        {
            string progress = $"-progress tcp://127.0.0.1:{FreeIPPort}";
            string logLevel = "-loglevel error";

            string AVCodecParams_mxf = "";
            string AVCodecParams_mp4 = "-movflags +faststart -s 640x360 -b:v 900000 -pix_fmt yuv420p -y -g 8 -c:v libx264 -profile:v baseline -level 3.2 -preset fast -crf 25 -c:a aac -ar 48000 -b:a 128k -ac 2 -f mp4";
            string mapping_mp4 = "-map 0:v -map [a1]";
            string mapping_mxf = "-c copy -map 0";

            //   string sourcefile = $@"{FastserveWorkerMain._exportLocation}\{Job.video_id}.mxf";
          //  string sourcefile = $@"\\storehlv\contenthlv$\Medex\MXF_Inbox\Media_to_MAM\fastserve_clips\{Job.video_id}.mxf";
            string sourcefile = $@"{_fastserve_clips}\{Job.Video_id}.mxf";
            // since the output is set to -ac 2 the 4 inputs of the merge filter are automatically downmixed to 2 tracks ( L = a:0+a:2 and R = a:1+a:3 )
            string filter_complex_mix = "[0:a:0][0:a:1][0:a:2][0:a:3] amerge=inputs=4[a1]";
            //string filter_complex_track1 = "[0:a:0][0:a:0]amerge=inputs=2[a2]";
            //string filter_complex_track2 = "[0:a:1][0:a:1]amerge=inputs=2[a3]";
            //string filter_complex_track3 = "[0:a:2][0:a:2]amerge=inputs=2[a4]";
            //string filter_complex_track4 = "[0:a:3][0:a:3]amerge=inputs=2[a5]";
            //string filter_complex_track5 = "[0:a:4][0:a:4]amerge=inputs=2[a6]";
            //string filter_complex_track6 = "[0:a:5][0:a:5]amerge=inputs=2[a7]";
            //string filter_complex_track7 = "[0:a:6][0:a:6]amerge=inputs=2[a8]";
            //string filter_complex_track8 = "[0:a:7][0:a:7]amerge=inputs=2[a9]";
            string t = "";

            string filter_complex_chain = $"-filter_complex \"{filter_complex_mix}\"";

            return $"-re -i \"{sourcefile}\" {logLevel} {progress} {mapping_mxf} {AVCodecParams_mxf} -f mxf -y \"{FastserveWorkerMain._exportLocation}\\{Job.Capture_clip_name}.mxf\" {filter_complex_chain} {mapping_mp4} {AVCodecParams_mp4} {t} -y \"{FastserveWorkerMain._exportLocation}\\{Job.Capture_clip_name}.mp4\"";
        }



        private void IngestProgressAndUpdateRenderJob(RenderJob Job, CancellationToken Token)
        {
            int? OldProgress = null;
            while (!Token.IsCancellationRequested)
            {
                if(Job.Job_progress != OldProgress)
                {
                    Console.WriteLine("prog: " + Job.Job_progress);
                    ProgressToIngestservice(Job);
                    OldProgress = Job.Job_progress;
                }


                if (mut.WaitOne(1000))
                {
                    RenderJob UpdatedJob = DatabaseIO.GetJob(Job.Job_id);
                    mut.ReleaseMutex();
                    if(UpdatedJob == null)
                    {
                        Thread.Sleep(30000);
                        continue;
                    }

                    Job.Growing = UpdatedJob.Growing;
                    Job.Length = UpdatedJob.Length;
                    Job.TargetLength = UpdatedJob.TargetLength;

                    int Length;
                    if (string.IsNullOrEmpty(UpdatedJob.TargetLength))
                    {
                        Length = TCFramesConverter.TCToSeconds(UpdatedJob.Length);
                    }
                    else
                    {
                        Length = TCFramesConverter.TCToSeconds(UpdatedJob.TargetLength);
                    }
                    _newlenghtObj.NewLenght = Length;
                }
                else
                {
                    Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "IngestProgressAndUpdateRenderJob: Mutex Release error");
                }

                Thread.Sleep(30000);

            }
        }

        private bool ProgressToIngestservice(RenderJob Job)
        {
            var restClient = new RestClient(_ingest_service_endpoint);
            var request = new RestRequest(Method.POST);
            request.AddParameter("Content-Type", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
            request.AddParameter("I_Airspeed_Filename", Job.Capture_clip_name + ".mxf", ParameterType.GetOrPost);
            request.AddParameter("I_Booking_Duration", TCFramesConverter.TCToFrames(String.IsNullOrEmpty(Job.TargetLength) ? Job.Length : Job.TargetLength).ToString(), ParameterType.GetOrPost);
            request.AddParameter("I_File_Path", Job.Export_location, ParameterType.GetOrPost);
            request.AddParameter("I_Progress", (Job.Job_progress ?? 0).ToString(), ParameterType.GetOrPost);
            request.AddParameter("I_Render_Service_ID", "0", ParameterType.GetOrPost);
            request.AddParameter("I_Start_TimeCode", Job.MasterTCIn, ParameterType.GetOrPost);
            request.AddParameter("I_Status", Job.Job_status.ToString(), ParameterType.GetOrPost);


            IRestResponse Response = restClient.Execute(request);

            if (Response.IsSuccessful == true)
            {
                //var sb = new StringBuilder();
                //foreach (var param in request.Parameters)
                //{
                //    sb.AppendFormat("{0}: {1}\r\n", param.Name, param.Value);
                //}
                //Logger.WriteConsoleAndLog(MsgType.DEBUG, null, sb.ToString());
            }
            else
            {
                Job.Job_error_description = "ProgressToIngestservice: " + Response.ErrorMessage;
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"ProgressToIngestservice: statuscode:{Response.StatusCode.ToString()} message:{Response.ErrorMessage}", Response.Content);
                DatabaseIO.UpdateJob(new RenderJob() { Job_id = Job.Job_id, Job_error_description = Job.Job_error_description });
            }
            
            return Response.IsSuccessful;
        }

        public static bool GetAvailablePort(out int FreePort, RenderJob Job)
        {
            int Range = 32;
            int StartRange = 8910 + (int) Job.Worker_id * Range;
            int[] ports = Enumerable.Range(StartRange, Range).ToArray();
            // Evaluate current system tcp serverside connections. 
            // We will look through the list, and if our port we would like to use
            // in our TcpListeners list is occupied, we will set isAvailable to false.

            foreach (int port in ports)
            {
                bool isAvailable = true;
                IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
                IPEndPoint[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpListeners();

                foreach (IPEndPoint endpoint in tcpConnInfoArray)
                {
                    if (endpoint.Port == port)
                    {
                        isAvailable = false;
                        break;
                    }
                }
                if (isAvailable)
                {
                    FreePort = port;
                    return true;
                }
            }
            Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "Getavailable TCP Ports: There are no available TCP ports left.");
            Job.Job_error_description = "Getavailable TCP Ports: There are no available TCP ports left.";
            DatabaseIO.UpdateJob(new RenderJob() { Job_id = Job.Job_id, Job_error_description = Job.Job_error_description });
            FreePort = 0;
            return false;
        }
    }
}

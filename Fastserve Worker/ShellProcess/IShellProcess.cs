﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastserve_Worker
{
    interface IShellProcess
    {
        string Command { get; set; }
        string Arguments { get; set; }
        int ExitCode { get; set; }
        StringBuilder Output { get; set; }
        StringBuilder Error { get; set; }
        bool TimeOut { get; set; }
        void Execute(int MaxRunMinutes, ConsoleCtrlEvent Event, bool Debug);
    }
}

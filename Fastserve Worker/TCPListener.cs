﻿using Newtonsoft.Json;
using SharedLib;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Fastserve_Worker
{
    class TCPTransmitter
    {
        public Boolean Transmit(String ip, String port, String data)
        {
            TcpClient client = new TcpClient();
            int.TryParse(port, out int _port);
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(ip), _port);
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] buffer = encoder.GetBytes(data);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            return true;
        }
    }

    class TCPListener
    {
        private LiveLenght _durObj;
        private TcpListener tcpListener;
        private Thread listenThread;
        Int32 port = 8910;
        private  RenderJob _job;

        public TCPListener(int port, RenderJob Job, LiveLenght DurObj)
        {
            _job = Job;
            _durObj = DurObj;
            Server(port);
        }

        public void Server(int Port)
        {
            port = Port;
            this.tcpListener = new TcpListener(IPAddress.Any, port);
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();
        }

        public void ListenForClients()
        {
            try
            {
                this.tcpListener.Start();
            }
            catch (SocketException ex)
            {
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Error while starting tcplistener. Message {ex.Message}", JsonConvert.SerializeObject(_job, Formatting.Indented));
                return;
            }


            var watch = System.Diagnostics.Stopwatch.StartNew();
            while (!tcpListener.Pending())
            {
                // Stop listener if FFMeg does not connect within 20 seconds.
                Thread.Sleep(1000);
                if (watch.ElapsedMilliseconds > 20000)
                {
                    watch.Stop();
                    Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"TcpListener: Tcplistener timed out after 20 seconds.", JsonConvert.SerializeObject(_job, Formatting.Indented));
                    tcpListener.Stop(); // <---- later toegvoegd , lijkt wel te werken
                    return;
                }
            }

            //blocks until a client has connected to the server
            TcpClient client = this.tcpListener.AcceptTcpClient();

            //create a thread to handle communication with connected client
            Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
            clientThread.Start(client);
        }

        private void HandleClientComm(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();
            clientStream.ReadTimeout = 130000;
            byte[] message = new byte[4096];
            int bytesRead;

            long oldProgress = 0;
            long newProgress = 0;

            while (true)
            {
                try
                {
                    //blocks max clientStream.ReadTimeout until a client sends a message
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch (Exception ex)
                {
                    //a socket error has occured
                    Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"Tcplistener: Socket error in HandleClientComm. " + ex.InnerException + ", " + ex.Message, JsonConvert.SerializeObject(_job, Formatting.Indented));
                    break;
                }

                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    break;
                }

                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();
                string text = encoder.GetString(message, 0, bytesRead);
                Match match = new Regex(@"(out_time_ms=)(\d+)").Match(text);

                if (BrowseRender.mut.WaitOne(1000))
                {
                    long TimeMS;
                    if (match.Success)
                    {
                        TimeMS = Convert.ToInt64(match.Groups[2].Value);
                        newProgress = ((TimeMS / 10000) / _durObj.NewLenght);
                        int newProgressClamp = (int)Math.Max(0, Math.Min(100, newProgress));  // force between 0 and 100
                        var Increment = (_durObj.NewLenght < 500) ? 2 : 1;                      // Progress every 2% voor small clips
                        if (Math.Abs(newProgress - oldProgress) >= Increment)
                        {
                            _job.Job_progress = newProgressClamp;
                            DatabaseIO.UpdateJob(new RenderJob() { Job_id = _job.Job_id, Job_progress = _job.Job_progress });
                            oldProgress = newProgress;
                        }
                    }
                    // the last progress line will be close to 100% but never will.
                    if (text.Contains("progress=end"))
                    {
                        _job.Job_progress = 100;
                        DatabaseIO.UpdateJob(new RenderJob() { Job_id = _job.Job_id, Job_progress = _job.Job_progress });
                    }

                    BrowseRender.mut.ReleaseMutex();
                }
                else
                {
                    Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, "TCPListener: Mutex Release error");
                }

   
#if DEBUG
                Console.Write($"\r{newProgress}  {_durObj.NewLenght.ToString()}     ");
#endif
            }

            Console.WriteLine("");
            tcpClient.Close();
            tcpListener.Stop(); // <---- later toegvoegd , lijkt wel te werken
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SharedLib;

namespace Fastserve_Worker
{
    public class FastserveWorkerMain
    {
        private readonly NameValueCollection _appSettings;
        static public int _worker = 1;
        static public string _exportLocation;
        static public string _fastserveClips;
        private string _hostname;
        private readonly int _pollCycle = 60 * 1000;

        public FastserveWorkerMain(string arg)
        {
            _worker = Convert.ToInt32(arg);
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            _pollCycle = Convert.ToInt32(_appSettings["poll_cycle_seconds"]) * 1000;
            _exportLocation = _appSettings["export_location"];
            _fastserveClips = _appSettings["fastserve_clips"];
            Logger._refID = System.Environment.MachineName;
            Logger.Connect();
            _hostname = System.Environment.MachineName + "." + System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToLower();
        }

        public void ConsoleStart()
        {
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Start App: {_hostname}. Worker: {_worker} ###############");
            Forever();
        }

        public void TaskStart()
        {
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Start Service: {_hostname}. Worker: {_worker} ###############");
            Task.Factory.StartNew(() => Forever());
        }

        public void TaskStop()
        {
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Stop Service:{_hostname}. Worker: {_worker} ###############");
            DatabaseIO.Close();
        }

        public void TaskShutdown()
        {
            Logger.WriteConsoleAndLogAndBus(MsgType.INFO, null, $"############### Shutdown Service:{_hostname}. Worker: {_worker} ###############");
            DatabaseIO.Close();
        }

        public void Forever()
        {
            CancellationTokenSource cancelSource = new CancellationTokenSource();
            try
            {
                do
                {
                    System.Configuration.ConfigurationManager.RefreshSection("appSettings");
                    bool Enabled = Convert.ToBoolean((System.Configuration.ConfigurationManager.AppSettings["enabled"]));
                    if(Enabled == true)
                    {
                        Logger.Connect();
                        Logger.RotateLog();
                        Start();
                    }
                    else
                    {
                        Logger.Disconnect();
                        DatabaseIO.Close();
                    }
                } 
                while (cancelSource.Token.WaitHandle.WaitOne(_pollCycle) || true);    // wait before diving into the loop again.
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", ex.ToString());
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", $"{ex.Message}");
                DatabaseIO.Close();
                Environment.Exit(1);
            }
        }


        public void Start()
        {
            RenderJob Job = DatabaseIO.GetNewJob();
            if (Job == null)
            {
                return;
            }
            Logger._refID = Job.Job_id.ToString();
            Job.Export_location = _exportLocation;
            Job.Video_id = Job.Video_id.Replace("%", "");  // Fastserve does not export % in Filename.
            Job.Worker_id = _worker;
            Job.Render_host_name = System.Environment.MachineName + "." + System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToLower();
            DatabaseIO.UpdateJob(new RenderJob() { Job_id = Job.Job_id, Worker_id = Job.Worker_id, Render_host_name = Job.Render_host_name, Job_status = 0 });

            if (File.Exists($"{_fastserveClips}\\{Job.Video_id}.mxf") == false)
            {
                Logger.WriteConsoleAndLogAndBus(MsgType.ERROR, null, $"FastServe Hires file {_fastserveClips}\\{Job.Video_id}.mxf does not exits (anymore)");
                Job.Job_status = Status.Error;
                Job.Job_error_description = $"FastServe Hires file {_fastserveClips}\\{Job.Video_id}.mxf does not exits (anymore)";
                DatabaseIO.UpdateJob(new RenderJob() { Job_id = Job.Job_id, Job_status = Job.Job_status, Job_error_description = Job.Job_error_description });
                return;
            }

            new BrowseRender(Job);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Fastserve_Worker
{
    static class TCFramesConverter
    {
        static public int TCToSeconds(string StrTimecode)
        {

            string pattern = @"^(\d\d):(\d\d):(\d\d):(\d\d)";
            Match m = Regex.Match(StrTimecode, pattern);
            if (m.Success)
            {
                int Hour = Convert.ToInt32(m.Groups[1].Value);
                int Minute = Convert.ToInt32(m.Groups[2].Value);
                int Seconds = Convert.ToInt32(m.Groups[3].Value);

                return (Hour * 3600 + Minute * 60 + Seconds);
            }
            else
            {
                return 1;
            }
        }


        static public long TCToFrames(string StrTimecode)
        {
            string pattern = @"^(\d\d):(\d\d):(\d\d):(\d\d)";
            Match m = Regex.Match(StrTimecode, pattern);
            if (m.Success)
            {
                int Hour = Convert.ToInt32(m.Groups[1].Value);
                int Minute = Convert.ToInt32(m.Groups[2].Value);
                int Seconds = Convert.ToInt32(m.Groups[3].Value);
                int Frames = Convert.ToInt32(m.Groups[4].Value);

                return (Hour * 3600 * 25 + Minute * 60 * 25 + Seconds * 25 + Frames);
            }
            else
            {
                return 1;
            }
        }
    }
}

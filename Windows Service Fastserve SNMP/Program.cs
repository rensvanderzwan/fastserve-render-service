﻿using System;
using System.ServiceProcess;



namespace Windows_Service_Fastserve_SNMP
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var service = new ServiceStarter(""))
            {
                ServiceBase.Run(service);
            }
        }
    }
}

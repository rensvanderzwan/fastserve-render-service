﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

// Install the service:
// sc create "Fastserve Manager4" binpath="C:\Source\Repos\Fastserve Render Service\WindowsService Fastserve Manager\bin\Release\netcoreapp2.2\win-x64\Windows Service Fastserve Manager.exe"

namespace Windows_Service_Fastserve_SNMP
{
    internal class ServiceStarter : ServiceBase
    {
        private Fastserve_SNMP.FastserveSNMPMain _renderService;

        public ServiceStarter(string StartupArg)
        {
            // InitializeComponent();
            _renderService = new Fastserve_SNMP.FastserveSNMPMain(StartupArg);
        }

        protected override void OnStart(string[] args)
        {
            _renderService.TaskStart();
        }

        protected override void OnStop()
        {
            _renderService.TaskStop();
        }

    }
}

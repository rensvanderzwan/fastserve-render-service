﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

namespace Windows_Service_Fastserve_Service_Starter_Service
{
    internal class ServiceStarter : ServiceBase
    {
        private Fastserve_Service_Starter_Service.ServiceStartAndStop _startStopService;

        public ServiceStarter(string StartupArg)
        {
            // InitializeComponent();
            _startStopService = new Fastserve_Service_Starter_Service.ServiceStartAndStop(StartupArg);
        }

        protected override void OnStart(string[] args)
        {
            _startStopService.TaskStart();
        }

        protected override void OnStop()
        {
            _startStopService.TaskStop();
        }

    }
}

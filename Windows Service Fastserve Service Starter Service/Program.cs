﻿using System;
using System.ServiceProcess;
// sc create "Fastserve Services Start" binpath="C:\Program Files\Fastserve Starter Service\Windows Service Fastserve Service Starter Service.exe Start"
// sc create "Fastserve Services Stop" binpath="C:\Program Files\Fastserve Starter Service\Windows Service Fastserve Service Starter Service.exe Stop"

namespace Windows_Service_Fastserve_Service_Starter_Service
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var service = new ServiceStarter(args[0]))
            {
                ServiceBase.Run(service);
            }

        }
    }
}

﻿using System;
using System.Threading.Tasks;
using sheditcap1;

namespace InterplayCaptureScheduler_hlv
{
    class Program
    {
        static async Task Main(string[] args)
        {

            ScheduleClientClient client = new ScheduleClientClient();
            userCredentials Credentials = new userCredentials() { userName = "caphvs", password = "NOS-edit" };

            Task<getChannelPoolsResponse> channelPoolsResponse = client.getChannelPoolsAsync(Credentials);
            channelPool[] channelpools = channelPoolsResponse.Result.@return;
            channelPool[] testpool = { new channelPool() { channelPool1 = "CH 1-24" } };

            Task<getSourcesResponse> GetSources = client.getSourcesAsync(Credentials);

            getSourcesResponse SourcesResponse = null;
            try
            {
                SourcesResponse = await GetSources;
            }
            catch (Exception ex)
            {
                // no license for exxample
                Console.WriteLine(ex.Message);
            }

            routerSource[] RSources = SourcesResponse.@return;
            //  routerSourceKey SomeSource = Array.Find(RSources, element => element.name == "NPO1-CF ").key;

            routerSourceKey[] routerSourceKeyPool = new routerSourceKey[24];
            routerSourceKeyPool[0] = Array.Find(RSources, element => element.name == "NPO1-CF ").key;
            routerSourceKeyPool[1] = Array.Find(RSources, element => element.name == "NPO2-CF ").key;
            routerSourceKeyPool[2] = Array.Find(RSources, element => element.name == "NPO3-CF ").key;
            routerSourceKeyPool[3] = Array.Find(RSources, element => element.name == "EVN     ").key;
            routerSourceKeyPool[4] = Array.Find(RSources, element => element.name == "REUTERS ").key;
            routerSourceKeyPool[5] = Array.Find(RSources, element => element.name == "APDIRECT").key;
            routerSourceKeyPool[6] = Array.Find(RSources, element => element.name == "CNN     ").key;
            routerSourceKeyPool[7] = Array.Find(RSources, element => element.name == "TEXT-TV1").key;

            routerSourceKeyPool[8] = Array.Find(RSources, element => element.name == "NPO1-CF ").key;
            routerSourceKeyPool[9] = Array.Find(RSources, element => element.name == "NPO2-CF ").key;
            routerSourceKeyPool[10] = Array.Find(RSources, element => element.name == "NPO3-CF ").key;
            routerSourceKeyPool[11] = Array.Find(RSources, element => element.name == "EVN     ").key;
            routerSourceKeyPool[12] = Array.Find(RSources, element => element.name == "REUTERS ").key;
            routerSourceKeyPool[13] = Array.Find(RSources, element => element.name == "APDIRECT").key;
            routerSourceKeyPool[14] = Array.Find(RSources, element => element.name == "CNN     ").key;
            routerSourceKeyPool[15] = Array.Find(RSources, element => element.name == "TEXT-TV1").key;

            routerSourceKeyPool[16] = Array.Find(RSources, element => element.name == "NPO1-CF ").key;
            routerSourceKeyPool[17] = Array.Find(RSources, element => element.name == "NPO2-CF ").key;
            routerSourceKeyPool[18] = Array.Find(RSources, element => element.name == "NPO3-CF ").key;
            routerSourceKeyPool[19] = Array.Find(RSources, element => element.name == "EVN     ").key;
            routerSourceKeyPool[20] = Array.Find(RSources, element => element.name == "REUTERS ").key;
            routerSourceKeyPool[21] = Array.Find(RSources, element => element.name == "APDIRECT").key;
            routerSourceKeyPool[22] = Array.Find(RSources, element => element.name == "CNN     ").key;
            routerSourceKeyPool[23] = Array.Find(RSources, element => element.name == "TEXT-TV1").key;

            int IncCounter = GetIncrement() + 1;
            int i;
            string YYMMDD = DateTime.Now.ToString("yyMMdd");
            for (i = 0; i < 4; i++)
            {
                int Parallel = 24; // number of simultanous recordings
                int Offset = 1; // first booking will be scheduled x minutes from now
                int Lenght = 3; // length of the recoding in minutes
                int Gap = 1; // Time between captures in minutes
                int Increments = (int)Math.Floor((double)i / Parallel);
                int postion = Increments * Lenght + Increments * Gap + Offset;  // relative to now
                int SourceKey = i % Parallel;
                int IncrementalOffset = 10; // Place each recording 5 seconds after the previous

                string HHMMSSFFFF = DateTime.Now.ToString("HHmmssffff");

                recordingParams recparams = new recordingParams()
                {
                    name = $"FastServe-TestClip{(i + IncCounter).ToString("D4")}-{HHMMSSFFFF}-CNI{YYMMDD}",
                    comments = "comments",
                    duration = Lenght * 60,
                 //   durationSpecified = true,
                    startDate = DateTime.Now.AddSeconds(postion * 60).AddSeconds(i * IncrementalOffset),
                    videoID = $"TstClp{(i + IncCounter).ToString("D4")}-{HHMMSSFFFF}-CNI{YYMMDD}",
                    routerSources = new routerSourceKey[] { routerSourceKeyPool[SourceKey] },
                    interplayFolder = "Projects/01 Scheduled Ingests",
                    tapeID = $"Altijd Handig een tapeID {HHMMSSFFFF} {(i + IncCounter)}",
                    startDateSpecified = true, // set to false for an almost instant recording
                    channelPool = testpool
                };


                //Task<addRecordingResponse> AddRecording = client.addRecordingAsync(Credentials, recparams, "Catalogs/_Capture templates/Nieuws.template");
                Task<addRecordingWithoutTemplatesResponse> AddRecording = client.addRecordingWithoutTemplatesAsync(Credentials, recparams);
                //addRecordingResponse AddRecordingResonse = await AddRecording;
                addRecordingWithoutTemplatesResponse AddRecordingResonse = await AddRecording;   // NO_TARGETS = Channelpool does not exist
                SetIncrement(IncCounter + i);
            }
        }

        static int GetIncrement()
        {
            try
            {
                string CounterText = System.IO.File.ReadAllText(@"C:\Temp\Counter.txt");
                return Convert.ToInt32(CounterText);
            }
            catch (Exception)
            {
                return 1;
            }
        }

        static void SetIncrement(int Increment)
        {
            try
            {
                System.IO.File.WriteAllText(@"C:\Temp\Counter.txt", Increment.ToString());
            }
            catch (Exception)
            {

            }
        }
    }
}

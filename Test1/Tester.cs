﻿using SharedLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Test1
{
    class Tester
    {
        private int avg = 0;
        private long sum = 0;
        string[] ids = { "20015b2a-a74b-1190-7101-404240900000", "20015b2a-a74b-1190-6081-107545400000", "20015b2a-a74b-1190-6081-107535000000", "20015b2a-a74b-1190-6081-107535200000" };
        public Tester()
        {
            Logger._LogPath = @"C:\Temp\Tester.log";
            int i = 0;
            while (true)
            { 
                foreach ( string id in ids)
                {
                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    XmlNodeList list = GetItemProperties("s-heditfast1.edit.nos.nl", Guid.Parse(id));
                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;
                    sum += elapsedMs;
                    avg = (int) sum / (i + 1);
                    Console.Write($"{avg} {elapsedMs} ");



                    Console.Write( list == null);
                    Console.WriteLine("");
                    Thread.Sleep(1500);
                    i++;
                }
            }
         
        }

        public XmlNodeList GetItemProperties(string Host, Guid Id)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"OCIPCommands\", "GetItemProperties.xml");
            string GetItemPropertiesCommand = File.ReadAllText(path);
            string SearchXML = BuildXML(GetItemPropertiesCommand, Id.ToString());

 
                XmlNodeList list =  GetItemPropertiesFromFastserve(Host, SearchXML);
        
            return list;

        }

        private string BuildXML(string XML, string SearchID)
        {
            // Check and double check
            // 20015b2a-a74b-1190-5301-538405600000
            if (Guid.TryParse(SearchID, out _) == false)
            {
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);
            XmlNode ItemNode = xmlDoc.SelectSingleNode("OCIP/Item");
            ItemNode.Attributes["Id"].Value = SearchID;

            return xmlDoc.OuterXml;
        }

        private XmlNodeList GetItemPropertiesFromFastserve(string Host, string SearchXML)
        {
            var Client = new socketClient();
            bool succes = Client.InitSocket(Host);
            if (!succes)
            {
                return null;
            }

            bool SendSucces = Client.Send(SearchXML, 0, SearchXML.Length, 15000);
            if (SendSucces == false)
            {
                return null;
            }

            string Received = Client.Receive();
            if (Received == "")
            {
                return null;
            }

            XmlNodeList Properties = ParseItemPropertiesXml(Received);
            if (Properties == null)
            {
                // try next time if parsing errors occur
                return null;
            }

            return Properties;
        }

        private XmlNodeList ParseItemPropertiesXml(string Xml)
        {
            // document has 3 root Nodes with the same name, so add a single root node to avoid parsing errors
            Xml = "<root>" + Xml + "</root>";
            XmlElement Root;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(Xml);
                Root = doc.DocumentElement;
            }
            catch (XmlException ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", "Error loading XML document: " + ex.Message);
                return null;
            }

            // The returnvalue of the Acknowledge Attribute should be "OK"
            string Acknowledge = Root.SelectSingleNode("//OCIP/Ack")?.Attributes["Return"]?.Value;
            if (Acknowledge != "OK")
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Acknowledge result is: {Acknowledge}");
                return null;
            }

            // The returnvalue of the Result Attribute should be "OK"
            string Result = Root.SelectSingleNode("//OCIP/Result")?.Attributes["Return"]?.Value;
            if (Result == "IdNotFound")
            {
                Logger.WriteConsoleAndLog(MsgType.WARN, "", $"ParseItemPropertiesXml: Result result is: {Result}");
                return null;
            }
            if (Result != "OK")
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Result is: {Result}");
                return null;
            }

            // The returnvalue of the Finished Attribute should be "OK"
            string Finished = Root.SelectSingleNode("//OCIP/Finished")?.Attributes["Return"]?.Value;
            if (Finished != "OK")
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"ParseItemPropertiesXml: Finished result is: {Finished}");
                return null;
            }

            // if the acknowledge and finished response is Ok than we should have 0 or more items in the Result
            XmlNodeList Items = Root.SelectNodes("//OCIP/Result/Item");

            return Items;
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using vsheditcaptst;

namespace InterplayCaptureScheduler_hlv
{
    class Program
    {
        static async Task Main(string[] args)
        {

            ScheduleClientClient client = new ScheduleClientClient();
            userCredentials Credentials = new userCredentials() { userName = "captst", password = "NOS-edit" };


            //try
            //{
            //    instantStartResponse1 instantStartResponse = await client.instantStartAsync(Credentials, new recordingID() { recordingID1 = "41bc4ec7-a45b-4005-8495-3408a4bc4e79" });
            //    instantStopResponse1 instantStopResponse = await client.instantStopAsync(Credentials, new recordingID() { recordingID1 = "41bc4ec7-a45b-4005-8495-3408a4bc4e79" });
            //}
            //catch(Exception ex)
            //{

            //}



            getChannelPoolsResponse channelPoolsResponse = await client.getChannelPoolsAsync(Credentials);
            channelPool[] channelpools = channelPoolsResponse.@return;
            channelPool[] testpool = { new channelPool() {  channelPool1 = "Test"} };

            Task<getSourcesResponse> GetSources = client.getSourcesAsync(Credentials);

            getSourcesResponse SourcesResponse = null;
            try
            {
                SourcesResponse = await GetSources;
            }
            catch (Exception ex)
            {
                // no license for exxample
                Console.WriteLine(ex.Message);
            }

            routerSource[] RSources = SourcesResponse.@return;
            //  routerSourceKey SomeSource = Array.Find(RSources, element => element.name == "NPO1-CF ").key;

            routerSourceKey[] routerSourceKeyPool = new routerSourceKey[8];
            routerSourceKeyPool[0] = Array.Find(RSources, element => element.name == "NPO1-CF").key;
            routerSourceKeyPool[1] = Array.Find(RSources, element => element.name == "NPO2-CF").key;
            routerSourceKeyPool[2] = Array.Find(RSources, element => element.name == "NPO3-CF").key;
            routerSourceKeyPool[3] = Array.Find(RSources, element => element.name == "EVN").key;
            routerSourceKeyPool[4] = Array.Find(RSources, element => element.name == "REUTERS").key;
            routerSourceKeyPool[5] = Array.Find(RSources, element => element.name == "APDIRECT").key;
            routerSourceKeyPool[6] = Array.Find(RSources, element => element.name == "CNN").key;
            routerSourceKeyPool[7] = Array.Find(RSources, element => element.name == "TEXT-TV1").key;

            int IncCounter = GetIncrement() + 1;
            int i;
            string YYMMDD = DateTime.Now.ToString("yyMMdd");
            for (i = 0; i < 8; i++)
            {
                int Parallel = 8; // number of simultanous recordings
                int Offset = 2; // first booking will be scheduled x minutes from now
                int Lenght = 4; // length of the recoding in minutes
                int Gap = 1; // Time between captures in minutes
                int Increments = (int)Math.Floor((double)i / Parallel);
                int postion = Increments * Lenght + Increments * Gap + Offset;  // relative to now
                int SourceKey = i % Parallel;
                int IncrementalOffset = 0; // Place each recording x seconds after the previous

                string HHMMSSFFFF = DateTime.Now.ToString("HHmmssffff");

                recordingParams recparams = new recordingParams()
                {
                    name = $"FastServe-Ingest-TestClip{(i + IncCounter).ToString("D4")}-{HHMMSSFFFF}-CNI{YYMMDD}",
                    comments = "comments",
                    duration = Lenght * 60,
                    durationSpecified = true,
                    startDate = DateTime.Now.AddSeconds(postion * 60).AddSeconds(i * IncrementalOffset),
                    videoID = $"TstClp{(i + IncCounter).ToString("D4")}-{HHMMSSFFFF}-CNI{YYMMDD}",
                    routerSources = new routerSourceKey[] { routerSourceKeyPool[SourceKey] },
                    interplayFolder = "Projects/01 Scheduled Ingests",
                    tapeID = $"Altijd Handig een tapeID {HHMMSSFFFF} {(i + IncCounter)}",
                    startDateSpecified = true, // set to false for an almost instant recording
                    channelPool = testpool
                };


                //Task<addRecordingResponse1> AddRecording = client.addRecordingAsync(Credentials, recparams, "Catalogs/_Capture templates/Nieuws.template");
                addRecordingWithoutTemplatesResponse1 AddRecording = await client.addRecordingWithoutTemplatesAsync(Credentials, recparams);

               var instantStart = await client.instantStartAsync(Credentials, new recordingID() { recordingID1 = AddRecording.@return.recordingID1 });
              //  addRecordingResponse1 AddRecordingResonse = await AddRecording;
            //    addRecordingWithoutTemplatesResponse1 AddRecordingResonse = await AddRecording;   // NO_TARGETS = Channelpool does not exist
                SetIncrement(IncCounter + i);
            }
        }

        static int GetIncrement()
        {
            try
            {
                string CounterText = System.IO.File.ReadAllText(@"C:\Temp\Counter.txt");
                return Convert.ToInt32(CounterText);
            }
            catch (Exception ex)
            {
                return 1;
            }
        }

        static void SetIncrement(int Increment)
        {
            try
            {
                System.IO.File.WriteAllText(@"C:\Temp\Counter.txt", Increment.ToString());
            }
            catch (Exception)
            {

            }
        }
    }
}

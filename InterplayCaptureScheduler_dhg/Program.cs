﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using vs_heditcap_tst;
using vsdeditcap1;

// http://s-ed-tcap1.edit.nos.nl:8080/ScheduleClient?wsdl


// Make N connections and request a listing on each of them
// On connection 33 and up the Fastseve terminates the connection 

namespace ScheduleClient1
{
    class Program
    {
        static async Task Main(string[] args)
        {

            ScheduleClientClient client = new ScheduleClientClient();
            userCredentials Credentials = new userCredentials() { userName = "capdh", password = "NOS-edit" };



            Task<getSourcesResponse> GetSources = client.getSourcesAsync(Credentials);

            getSourcesResponse SourcesResponse = null;
            try
            {
                SourcesResponse = await GetSources;
            }
            catch(Exception ex)
            {
                // no license for exxample
                Console.WriteLine(ex.Message);
            }
            
            routerSource[] RSources = SourcesResponse.@return;
          //  routerSourceKey SomeSource = Array.Find(RSources, element => element.name == "NPO1-CF ").key;

            routerSourceKey[] routerSourceKeyPool = new routerSourceKey[8];
            routerSourceKeyPool[0] = Array.Find(RSources, element => element.name == "2K NOS 1").key;  
            routerSourceKeyPool[1] = Array.Find(RSources, element => element.name == "DAKCAM  ").key;  
            routerSourceKeyPool[2] = Array.Find(RSources, element => element.name == "2K NOS 1").key;  
            routerSourceKeyPool[3] = Array.Find(RSources, element => element.name == "EXT 1   ").key;  
            routerSourceKeyPool[4] = Array.Find(RSources, element => element.name == "2K NOS 1").key;  
            routerSourceKeyPool[5] = Array.Find(RSources, element => element.name == "DAKCAM  ").key;  
            routerSourceKeyPool[6] = Array.Find(RSources, element => element.name == "EXT 1   ").key;  
            routerSourceKeyPool[7] = Array.Find(RSources, element => element.name == "2K NOS 1").key; 

            int IncCounter = GetIncrement() + 1;
            int i;
            string YYMMDD = DateTime.Now.ToString("yyMMdd");
            for (i = 0; i < 2 ; i++)
            {
                int Parallel = 8; // number of simultanous recordings
                int Offset = 2; // first booking will be scheduled x minutes from now
                int Lenght = 30; // length of the recoding in minutes
                int Gap = 1; // Time between captures in minutes
                int Increments = (int)Math.Floor((double)i / Parallel);
                int postion = Increments * Lenght + Increments * Gap + Offset;  // relative to now
                int SourceKey = i % Parallel;
                int IncrementalOffset = 5; // Place each recording 5 seconds after the previous

                string HHMMSSFFFF = DateTime.Now.ToString("HHmmssffff");

                recordingParams recparams = new recordingParams()
                {
                    name = $"FastServe-TestClip{(i + IncCounter).ToString("D4")}-{HHMMSSFFFF}-CNI{YYMMDD}",
                //    comments = "comments",
                    duration = Lenght * 60,
               //     durationSpecified = true,
                    startDate = DateTime.Now.AddSeconds(postion * 60).AddSeconds(i * IncrementalOffset),
                    videoID = $"TstClp{(i + IncCounter).ToString("D4")}-{HHMMSSFFFF}-CNI{YYMMDD}",
                    routerSources = new routerSourceKey[] { routerSourceKeyPool[SourceKey] },
                    interplayFolder = "Projects/01 Scheduled Ingests",
                 //   tapeID = $"Altijd Handig een tapeID {HHMMSSFFFF} {(i+IncCounter)}",
                    startDateSpecified = true // set to false for an almost instant recording
                };


                Task<addRecordingResponse> AddRecording = client.addRecordingAsync(Credentials, recparams, "Catalogs/_Capture templates/Nieuws.template");
                addRecordingResponse AddRecordingResonse = await AddRecording;
                SetIncrement(IncCounter + i);
            }
        }

        static int GetIncrement()
        {
            try
            {
                string CounterText = System.IO.File.ReadAllText(@"C:\Temp\Counter.txt");
                return Convert.ToInt32(CounterText);
            }
            catch (Exception)
            {
                return 1;
            }
        }

        static void SetIncrement(int Increment)
        {
            try
            {
                System.IO.File.WriteAllText(@"C:\Temp\Counter.txt", Increment.ToString());
            }
            catch(Exception)
            {

            }
        }
    }
}
